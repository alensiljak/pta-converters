#!/usr/local/bin/python
"""
original script from https://github.com/MatzeB/pygnucash/blob/master/gnucash2ledger.py 
by Matthias Braun matze@braunis.de. Adapted for python 3 support and new string formatting by sdementen
in piecash.
Additional options for splitting the file and filtering records by Alen Siljak.
"""
from __future__ import unicode_literals
import argparse
import sys
import codecs
from piecash import Transaction, Account, Commodity, Price, Book, open_book
import click
#from piecash.scripts.cli import cli


@click.command()
@click.argument('book', type=click.Path(exists=True))
@click.option('--output', type=click.File('w', encoding="utf-8"), default="-",
              help="File to which to export the data (default=stdout)")
@click.option('--commodities', default=False, is_flag=True)
@click.option('--accounts', default=False, is_flag=True)
@click.option('--prices', default=False, is_flag=True)
@click.option('--journal', default=False, is_flag=True)
@click.option('--with-exchange', default=False, is_flag=True)
def ledger(book, output, commodities, accounts, prices, journal, with_exchange):
    """
    Export to ledger-cli format.
    This scripts export a GnuCash BOOK to the ledget-cli format.
    """
    # If none of the parts are specified, return all. 
    if (commodities or accounts or prices or journal) == False:
        commodities = True
        accounts = True
        prices = True
        journal = True

    with open_book(book, open_if_lock=True) as data:
        #output.write(piecash.ledger(data, commodities))
        result = get_ledger_output(data, commodities, accounts, prices, journal,
        	with_exchange)
        output.write(result)


class GnuCash2LedgerParser:
    """ Parses GnuCash entities into Ledger format """
    def __init__(self):
        self._symbol_with_exchange = False

    def parse_commodity(self, commodity):
        if commodity.mnemonic in ["", "template"]:
            return ""

        res = "commodity {}\n".format(self.format_commodity(commodity))

        if commodity.fullname != "":
            res += "\tnote {}\n".format(commodity.fullname)

        res += "\n"

        return res

    def format_commodity(self, commodity: Commodity):
        """
        Format the commodity display. Currencies use only symbol.
        Other commodities can be output with or without the namespace (exchange)
        by setting the _symbol_with_exchange to True.
        """
        if not self._symbol_with_exchange or commodity.namespace == "CURRENCY":
            symbol = commodity.mnemonic
        else:
            # Python 3.6 syntax
            #symbol = f"{commodity.namespace}.{commodity.mnemonic}"
            symbol = "{}.{}".format(commodity.namespace, commodity.mnemonic)

        try:
            if symbol.encode('ascii').isalpha():
                return symbol
        except:
            pass
        return "\"{}\"".format(symbol)  # TODO: escape " char in symbol

    def parse_transaction(self, tr):
        """Return a ledger-cli alike representation of the transaction"""
        s = ["{:%Y-%m-%d} {}{}\n".format(tr.post_date,
                                        "({}) ".format(tr.num.replace(")", "")) if tr.num else "",
                                        tr.description)]
        if tr.notes:
            s.append("\t;{}\n".format(tr.notes))
        for split in tr.splits:
            if split.account.commodity.mnemonic == "template":
                return ""
            if split.reconcile_state in ['c', 'y']:
                s.append("\t* {:38}  ".format(split.account.fullname))
            else:
                s.append("\t{:40}  ".format(split.account.fullname))
            if split.account.commodity != tr.currency:
                s.append("{:10.{}f} {} @@ {:.{}f} {}".format(
                    split.quantity,
                    split.account.commodity.precision,
                    self.format_commodity(split.account.commodity),
                    abs(split.value),
                    tr.currency.precision,
                    self.format_commodity(tr.currency)))
            else:
                s.append("{:10.{}f} {}".format(split.value,
                                            tr.currency.precision,
                                            self.format_commodity(tr.currency)))
            if split.memo:
                s.append(" ;   {:20}".format(split.memo))
            s.append("\n")

        return "".join(s)

    def parse_price(self, price):
        """Return a ledger-cli alike representation of the price"""
        return "P {:%Y-%m-%d %H:%M:%S} {} {} {}\n".format(price.date,
                                                        self.format_commodity(price.commodity),
                                                        price.value,
                                                        self.format_commodity(price.currency))

    def parse_account(self, acc):
        """Return a ledger-cli alike representation of the account"""
        # ignore "dummy" accounts
        if acc.type is None or acc.parent is None:
            return ""
        if acc.commodity.mnemonic == "template":
            return ""
        res = "account {}\n".format(acc.fullname, )
        if acc.description != "":
            res += "\tnote {}\n".format(acc.description, )

        res += "\tcheck commodity == \"{}\"\n".format(
            self.format_commodity(acc.commodity).replace("\"", "\\\""))
        return res


def get_ledger_output(book, commodities=True, accounts=True, prices=True, journal=True,
    with_exchange=False):
    """ Returns the specified parts of the book in ledger format """
    res = []

    parser = GnuCash2LedgerParser()
    parser._symbol_with_exchange = with_exchange

    # Commodities
    if commodities:
        res.append("; Commodities\n")
        for commodity in book.commodities:
            #res.append(ledger(commodity))
            res.append(parser.parse_commodity(commodity))
        res.append("\n")

    # Accounts
    if accounts:
        res.append("; Accounts\n")
        for acc in book.accounts:
            #res.append(ledger(acc))
            res.append(parser.parse_account(acc))
            res.append("\n")

    # Prices
    if prices:
        res.append("; Prices\n")
        for price in sorted(book.prices, key=lambda x: x.date):
            #res.append(ledger(price))
            res.append(parser.parse_price(price))
        res.append("\n")

    # Transactions
    if journal:
        res.append("; Transactions\n")
        for trans in sorted(book.transactions, key=lambda x: x.post_date):
            #res.append(ledger(trans))
            res.append(parser.parse_transaction(trans))
            res.append("\n")

    return "".join(res)    

if __name__ == '__main__':
    ledger()