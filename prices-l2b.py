'''
Convert ledger prices to Beancount:
P 2017-08-23 00:00:00 WBCHA 100 AUD
2017-08-23 price WBCHA  100 AUD
'''
import click

def convert_price(ledger_price: str) -> str:
    ''' Convert one price line '''
    parts = ledger_price.split(" ")

    statement = parts[0]
    date = parts[1]

    if len(parts) == 6:
        # we have the time
        time = parts[2]
        i = 3

    if len(parts) == 5:
        # no time
        pass

    commodity = parts[i]
    commodity = commodity.replace('"', '')  # remove double-quotes

    i += 1
    value = parts[i]
    i += 1
    currency = parts[i]
    # currency = currency.strip()

    #return " ".join(parts)
    return f"{date} price {commodity} {value} {currency}"

def convert_file(ledger_path: str) -> str:
    '''
    Convert the price file.
    '''
    ledger_file = open(ledger_path)

    for in_line in ledger_file:
        if in_line.startswith(";"):
            print(in_line)
            continue
        
        in_line = in_line.strip()
        if not in_line:
            continue

        out_line = convert_price(in_line)
        print(out_line)

    ledger_file.close()


@click.command()
@click.argument('ledger', type=click.Path(exists=True)) # help="Ledger prices file"
#@click.argument('bc', type=click.Path(exists=True), help="Beancount prices file")
def convert(ledger):
    convert_file(ledger)

if __name__ == "__main__":
    convert()
