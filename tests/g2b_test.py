'''
Test the beancount export script
'''
#import sys
import importlib.util
import click
from click.testing import CliRunner

#sys.path.append('../')

def test_greet():
    @click.command()
    @click.argument('name')
    def greet(name):
        click.echo('Hello %s' % name)

    runner = CliRunner()
    result = runner.invoke(greet, ['Sam'])
    assert result.output == 'Hello Sam\n'

def test_beancount_export():
    # import the script
    spec = importlib.util.spec_from_file_location("module.name", "../gnucash2beancount.py")
    foo = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(foo)

    book = "p:\\My Documents\\finance\\GnuCash\\alen.sql.gnucash"
    output = "test.bean"
    commodities = True
    accounts = True
    prices = True
    journal = True
    with_exchange = False

    with open(output, "w", encoding="utf-8") as f:
        foo.export_internal(book, f, commodities, accounts, prices, journal, with_exchange)

if __name__ == '__main__':
    test_beancount_export()
