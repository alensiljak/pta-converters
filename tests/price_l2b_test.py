'''
Test the beancount price conversion
'''
import importlib.util
import click
from click.testing import CliRunner

def test_conversion():
    import contextlib

    spec = importlib.util.spec_from_file_location("module.name", "../prices-l2b.py")
    export = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(export)

    input = "p:/My documents/finance/ledger/prices/prices.ledger"
    output = "prices.bean"

    with open(output, "w", encoding="utf-8") as f:
        with contextlib.redirect_stdout(f):
            export.convert_file(input)
        # f.write(result)

if __name__ == '__main__':
    test_conversion()
