#!/usr/local/bin/python
'''
Script for exporting GnuCash book to Beancount.
Due to the lack of cost basis information in GnuCash, it is recommended to use None booking method 
in Beancount.

ToDo:
- reconciliation state for transaction
- price for commodity trade?
'''
from __future__ import unicode_literals
import argparse
import sys
import codecs
from piecash import Transaction, Account, Commodity, Price, Book, open_book
import click


@click.command()
@click.argument('book', type=click.Path(exists=True))
@click.option('--output', type=click.File('w', encoding="utf-8"), default="-",
              help="File to which to export the data (default=stdout)")
@click.option('--commodities', default=False, is_flag=True)
@click.option('--accounts', default=False, is_flag=True)
@click.option('--prices', default=False, is_flag=True)
@click.option('--journal', default=False, is_flag=True)
@click.option('--with-exchange', default=False, is_flag=True)
def export(book, output, commodities, accounts, prices, journal, with_exchange):
    """
    Export to ledger-cli format.
    This scripts export a GnuCash BOOK to the ledget-cli format.
    """
    # If none of the parts are specified, return all. 
    if (commodities or accounts or prices or journal) == False:
        commodities = True
        accounts = True
        prices = True
        journal = True

    export_internal(book, output, commodities, accounts, prices, journal, with_exchange)


class GnuCash2BeancountParser:
    """ Parses GnuCash entities into Bancount format """
    def __init__(self):
        self._symbol_with_exchange = False

    def parse_commodity(self, commodity):
        ''' commodity directive '''
        if commodity.mnemonic in ["", "template"]:
            return ""

        commodity_name = self.format_commodity(commodity)
        date = "1970-01-01" # todo use a different date?

        res = f"{date} commodity {commodity_name}\n"

        if commodity.fullname != "":
            res += f"\tname: \"{commodity.fullname}\"\n"

        res += "\n"

        return res

    def parse_transaction(self, tr):
        """Return a beancount representation of the transaction"""
        # s = ["{:%Y-%m-%d} {}{}\n".format(tr.post_date,
        #                                 "({}) ".format(tr.num.replace(")", "")) if tr.num else "",
        #                                 tr.description)]
        date = tr.post_date
        notes = ""
        if tr.notes:
            notes = self.format_notes(tr.notes)

        s = f'{date:%Y-%m-%d} * "{tr.description}" "{notes}"\n'

        for split in tr.splits:
            if split.account.commodity.mnemonic == "template":
                return ""

            account_name = self.format_account_name(split.account.fullname)

            if split.reconcile_state in ['c', 'y']:
                s += f"\t* {account_name:38}  "
            else:
                s += f"\t{account_name:40}  "

            # Amount

            if split.account.commodity != tr.currency:
                # The split is in different currency than the transaction.

                s += self.get_split_amount_as_cost_basis(split, tr)
            else:
                # The split is in the same currency as the transaction.
                s += "{:10.{}f} {}".format(split.value,
                                            tr.currency.precision,
                                            self.format_commodity(tr.currency))
            if split.memo:
                s += " ;   {:20}".format(split.memo)
            s += "\n"

        return s

    def parse_price(self, price):
        """Return a beancount-alike representation of the price"""
        commodity = self.format_commodity(price.commodity)
        currency = self.format_commodity(price.currency)
        return f"{price.date:%Y-%m-%d} price {commodity}  {price.value} {currency}\n"
        # %H:%M:%S

    def parse_account(self, acc):
        ''' Return an account directive '''
        # ignore "dummy" accounts
        if acc.type is None or acc.parent is None:
            return ""
        if acc.commodity.mnemonic == "template":
            return ""

        # Use min epoch date by default. Should be custom set.
        open_date = "1970-01-01"
        commodity = self.format_commodity(acc.commodity)
        account_name = self.format_account_name(acc.fullname)

        # ignore root accounts
        if not ":" in account_name:
            return ""

        res = f"{open_date} open {account_name}  {commodity}\n"

        # if acc.description != "":
        #     res += "\tnote {}\n".format(acc.description, )

        # res += "\tcheck commodity == \"{}\"\n".format(
        #     self.format_commodity(acc.commodity).replace("\"", "\\\""))
        return res

    def capitalize_sections(self, account_name: str) -> str:
        ''' Capitalize names of all account sections '''
        sections = account_name.split(":")
        result = []
        for section in sections:
            if not section[0].isupper():
                #section = section.title()
                section = section[0].upper() + section[0:]
            result.append(section)
        
        return ":".join(result)

    def format_account_name(self, full_name: str) -> str:
        ''' Format account name '''
        # replace special chars
        name = full_name.replace(" ", "-")
        name = name.replace('.', "-")
        name = name.replace(',', "-")

        name = name.replace('@', 'at')
        name = name.replace('%', 'pct')
        # name = name.replace('.', 'dot')
        name = name.replace('&', 'and')
        name = name.replace('+', 'plus')
        name = name.replace('?', 'q')
        name = name.replace('¢', 'cent')
        
        # Beancount does not allow Umlauts
        name = name.replace('ä', 'ae')
        name = name.replace('Ä', 'Ae')
        name = name.replace('ö', 'oe')
        name = name.replace('Ö', 'Oe')
        name = name.replace('ü', 'ue')
        name = name.replace('Ü', 'Ue')

        name = name.replace("'", '')  # Joe's -> Joes
        name = name.replace('(', '')
        name = name.replace(')', '')  # 401(k) -> 401k
        # name = name.replace(',', '')  # A, B & C -> A-B-and-C
        name = name.replace('*', '')  # Macy*s -> Macys

        name = name.replace('/', '-')
        name = name.replace('_', '-')

        # The above may have created names with multiple dashes
        while True:
            n = name.replace('--', '-')
            if n == name: break
            name = n

        # handle Trading accounts.
        if name.startswith("Trading:"):
            name = "Equity:" + name
        # Capitalize the first letter of each section.
        name = self.capitalize_sections(name)

        return name

    def format_notes(self, memo: str) -> str:
        ''' format notes '''
        return memo.replace('"', '\\"')

    def format_commodity(self, commodity: Commodity):
        """
        Format the commodity display. Currencies use only symbol.
        Other commodities can be output with or without the namespace (exchange)
        by setting the _symbol_with_exchange to True.
        """
        if not self._symbol_with_exchange or commodity.namespace == "CURRENCY":
            symbol = commodity.mnemonic
        else:
            # Python 3.6 syntax
            symbol = f"{commodity.namespace}.{commodity.mnemonic}"

        # try:
        #     if symbol.encode('ascii').isalpha():
        #         return symbol
        # except:
        #     pass
        # return "\"{}\"".format(symbol)
        return symbol

    def get_split_amount_as_total(self, split, tr):
        ''' Format the amount as the total cost, @@ syntax.
        The original output for ledger. '''
        # s += "{:10.{}f} {} @@ {:.{}f} {}".format(
        #     split.quantity,
        #     split.account.commodity.precision,
        #     commodity,
        #     abs(split.value),
        #     tr.currency.precision,
        #     currency)

        commodity_precision = split.account.commodity.precision

        result = "{:10.{}f} {} @@ {:.{}f} {}".format(
                            split.quantity,
                            commodity_precision,
                            self.format_commodity(split.account.commodity),
                            abs(split.value),
                            tr.currency.precision,
                            self.format_commodity(tr.currency))
        return result

    def get_split_amount_as_lot_and_total(self, split, tr):
        ''' The original format with {} added to establish a lot '''
        commodity = self.format_commodity(split.account.commodity)
        currency = self.format_commodity(tr.currency)

        result = f"{split.quantity:10.{split.account.commodity.precision}f} "
        result += f" {commodity}"
        result += f" {{}}"
        result += f" @@"
        result += f" {abs(split.value):{tr.currency.precision}f}"
        result += f" {currency}"

        return result

    def get_split_amount_as_cost_basis(self, split, tr):
        '''
        Formats the amount as the cost basis.
        '''
        # todo use the price syntax only for non-currency trades.
        is_currency = (split.account.commodity.namespace == "CURRENCY" or 
            split.quantity == 0 or split.value == 0)
        commodity = self.format_commodity(split.account.commodity)
        currency = self.format_commodity(tr.currency)
        
        if not is_currency:
            price = abs(split.value) / abs(split.quantity)

        s = f"{split.quantity:10.{split.account.commodity.precision}f} {commodity}"

        if not is_currency:
            # Share trade.
            #s += f" @ {price} {currency}"
            if split.quantity > 0:
                # purchase
                s += f" {{ {price} {currency} }}"
            else:
                #sale. Use FIFO?
                # s += f" {{}}"
                s += f" {{ {price} {currency} }}"
        else:
            s += f" @@ {abs(split.value):{tr.currency.precision}f} {currency}"

        return s

def export_internal(book, output, commodities, accounts, prices, journal, with_exchange):
    with open_book(book, open_if_lock=True) as data:
        result = get_output(data, commodities, accounts, prices, journal,
        	with_exchange)
        output.write(result)


def get_output(book: Book, commodities=True, accounts=True, prices=True, journal=True,
    with_exchange=False):
    """ Returns the specified parts of the book in ledger format """
    res = []

    parser = GnuCash2BeancountParser()
    parser._symbol_with_exchange = with_exchange

    # Operating currency. Export only if exporting the whole book.
    if commodities and accounts and prices and journal:
        operating_currency = book.default_currency.mnemonic
        res.append(f'option "operating_currency" "{operating_currency}"\n')
        
    # Commodities
    if commodities:
        res.append("; Commodities\n")
        for commodity in book.commodities:
            #res.append(ledger(commodity))
            res.append(parser.parse_commodity(commodity))
        res.append("\n")

    # Accounts
    if accounts:
        res.append("; Accounts\n")
        for acc in book.accounts:
            #res.append(ledger(acc))
            res.append(parser.parse_account(acc))
            res.append("\n")

    # Prices
    if prices:
        res.append("; Prices\n")
        for price in sorted(book.prices, key=lambda x: x.date):
            #res.append(ledger(price))
            res.append(parser.parse_price(price))
        res.append("\n")

    # Transactions
    if journal:
        res.append("; Transactions\n")
        for trans in sorted(book.transactions, key=lambda x: x.post_date):
            #res.append(ledger(trans))
            res.append(parser.parse_transaction(trans))
            res.append("\n")

    return "".join(res)    

if __name__ == '__main__':
    export()
