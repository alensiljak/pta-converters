# Conversion Table

https://www.tablesgenerator.com/markdown_tables


| From / To      |     BeanCount    |   CSV   | GnuCash |     Ledger     | MoneyManagerEx | Quicken |
|----------------|:----------------:|:-------:|:-------:|:--------------:|:--------------:|:-------:|
| BeanCount      |         X        |         |         |   bean-report  |                |         |
| CSV            |                  |    X    |         |                |                |         |
| GnuCash        |  pta-converters  |         |    X    | pta-converters |                |         |
| Ledger         | ledger2beancount |         |         |        X       |                |         |
| MoneyManagerEx |                  | MMExLib |         |                |        X       |         |
| Quicken        | QifParse         |         |         |                |                |    X    |

- pta-converters = this project
- ledger2beancount
- [QifParse](https://github.com/blais/qifparse)
