# pta-converters

Plain-Text Accounting conversion tools

See [Conversion](conversion.md) file for a general list of tools.

This package contains scripts for:

- GnuCash -> ledger
- GnuCash -> beancount
- prices: ledger -> beancount
